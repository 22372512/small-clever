/*Javascript file which will handle the inputs for the website.
*functions to assign the data in the input boxes to a variable, using get element by id and .vaule to get the information in forms.
*A variable that will store the url link, makes the code tider by not having to use the long html twice.
*The next document get element by id gets the a href in the html, uses innerhtml to change that to the url that was generated.
*Using a try and catch to copy the url link.
*/
function input() {
    //Gets the three inputs from the form 
    var inputone = document.getElementById("one").value;
    var inputtwo = document.getElementById("two").value;
    var inputthree = document.getElementById("three").value;
    //Builds the return link for the user.
    var urllink =  "http://localhost:3000/ww/form_builder/forms/14/completions/new?programme_id=" + inputone + "&channel_id="+ inputtwo + "&redirect_to=/" + inputthree;
    //Writes the link on the web page.
    document.getElementById("link").innerHTML = urllink;
    
    //Try and catch for copy the link. 
    try{
        //Use execcommand to copy the link
        document.execCommand('Copy', true, urllink);
        console.log('Copy to clipboard was successfull');
    }catch(err){
        //If it fails then console log the error.
        console.log('Not able to copy the link');
   }

}